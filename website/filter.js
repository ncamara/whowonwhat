console.log("page loaded");

var yearsubmitButton = document.getElementById("year-submit-button");
yearsubmitButton.onmouseup = getyearInfo;
var awardsubmitButton = document.getElementById("award-submit-button");
awardsubmitButton.onmouseup = getawardInfo;
var winssubmitButton = document.getElementById("wins-submit-button");
winssubmitButton.onmouseup = getwinsInfo;
var nomsubmitButton = document.getElementById("nom-submit-button");
nomsubmitButton.onmouseup = getnomInfo;
var entitysearchButton = document.getElementById("entity-submit-button");
entitysearchButton.onmouseup = getentityInfo;
var yearreset = document.getElementById("year-clear-button");
yearreset.onmouseup = yearclear;
var awardreset = document.getElementById("award-clear-button");
awardreset.onmouseup = awardclear;
var winsreset = document.getElementById("wins-clear-button");
winsreset.onmouseup = winsclear;
var nomreset = document.getElementById("nom-clear-button");
nomreset.onmouseup = nomclear;
var entityreset = document.getElementById("entity-clear-button");
entityreset.onmouseup = entityclear;



function yearclear(){
	var filter_results = document.getElementById('year-results');
        filter_results.innerHTML = "";
}

function awardclear(){
        var filter_results = document.getElementById('award-results');
        filter_results.innerHTML = "";
}

function winsclear(){
        var filter_results = document.getElementById('wins-results');
        filter_results.innerHTML = "";
}

function nomclear(){
        var filter_results = document.getElementById('nom-results');
        filter_results.innerHTML = "";
}

function entityclear(){
	var filter_results = document.getElementById('entity-results');
        filter_results.innerHTML = "";
}

function getentityInfo(){
        console.log("entered getentityInfo");
        var entity_text = document.getElementById('entity-text').value;
        makecallEntity(entity_text);

}

function makecallEntity(entity){
        console.log('entered makecallentity');
        var url = 'http://student12.cse.nd.edu:51046/oscars/';
        var xhr = new XMLHttpRequest();
        xhr.open("GET", url, true);

        xhr.onload = function(e){
                console.log('response received');
                displayentity(xhr.responseText, entity);
        }
        xhr.onerror = function(e){
                console.error(xhr.statusText);
        }
        xhr.send(null);


}

function displayentity(response, entity){
        console.log("entered display", response);
        var resp_json = JSON.parse(response);
        var resp_array = resp_json['data'];
        var resp_str = processArraysentity(resp_array, entity);
        var filter_results = document.getElementById('entity-results');
        filter_results.innerHTML = resp_str;

}

function processArraysentity(resp_array, entity){
        var resp_str = "";
        for(var i = 0; i < resp_array.length; i++){
                var resp_obj = resp_array[i];
                if(resp_obj['entity'] == entity){
			if(resp_obj['winner'] == 'TRUE'){
				resp_str += 'Won ' + resp_obj['category'] + ' in ' + resp_obj['year'] + '<br><hr>' ;
			}
			else{
				resp_str += 'Nominated for ' + resp_obj['category'] + ' in ' + resp_obj['year'] + '<br><hr>' ;
			}
		}
        }
        return resp_str;

}





function getyearInfo(){
        console.log("entered getFormInfo");
	var year_text = document.getElementById('year-text').value;
	makecallYear(year_text);
}

function getawardInfo(){
        console.log("entered getFormInfo");
	var award_text = document.getElementById('award-text');
	var award_resp = award_text.options[award_text.selectedIndex].value
	makecallAward(award_resp);
}

function getwinsInfo(){
        console.log("entered getFormInfo");
	var wins_text = document.getElementById('wins-text').value;
	makecallWins(wins_text);

}

function getnomInfo(){
        console.log("entered getFormInfo");
	var nom_text = document.getElementById('nom-text').value;
	makecallNom(nom_text);

}

function makecallYear(year){
	console.log('entered makecallyear');
	var url = 'http://student12.cse.nd.edu:51046/oscars/year/' + parseInt(year);
	var xhr = new XMLHttpRequest();
	xhr.open("GET", url, true);

	xhr.onload = function(e){
		console.log('response received');
		displayyear(xhr.responseText);
	}
	xhr.onerror = function(e){
		console.error(xhr.statusText);
	}
	xhr.send(null);


}

function displayyear(response){
	console.log("entered display", response);
	var resp_json = JSON.parse(response);
	var resp_array = resp_json['data'];
	var resp_str = processArraysyear(resp_array);
	var filter_results = document.getElementById('year-results');
	filter_results.innerHTML = resp_str;

}

function processArraysyear(resp_array){
	var resp_str = "";
	for(var i = 0; i < resp_array.length; i++){
		var resp_obj = resp_array[i];
		if(resp_obj['winner'] == 'TRUE'){
			resp_str += resp_obj['entity'] + ' won ' + resp_obj['category'] + '<br><hr>' ;
		}
		else{
			resp_str += resp_obj['entity'] + ' was nominated for ' + resp_obj['category'] + '<br><hr>';
		}
	}
	return resp_str;

}

function makecallAward(award){
	console.log('entered makecallaward');
        var url = 'http://student12.cse.nd.edu:51046/oscars/';
        var xhr = new XMLHttpRequest();
        xhr.open("GET", url, true);

        xhr.onload = function(e){
                console.log('response received');
		var data = (xhr.responseText);
                displayAward(data, award);
        }
        xhr.onerror = function(e){
                console.error(xhr.statusText);
        }
        xhr.send(null);

}


function displayAward(response, award){
        console.log("entered display", response);
        var resp_json = JSON.parse(response);
        var resp_array = resp_json['data'];
        var resp_str = processArraysAward(resp_array, award);
        var filter_results = document.getElementById('award-results');
        filter_results.innerHTML = resp_str;

}

function processArraysAward(resp_array, award){
        var resp_str = "";
        for(var i = 0; i < resp_array.length; i++){
                var resp_obj = resp_array[i];
		if(resp_obj['category'] == award){
			if(resp_obj['winner'] == 'TRUE'){
				resp_str += resp_obj['entity'] + " won in " + resp_obj['year'] + '<br><hr>';
			}
			else{
				resp_str += resp_obj['entity'] + " was nominated in " + resp_obj['year'] + '<br><hr>';
			}
		}
        }
        return resp_str;


}


function makecallWins(wins){
	console.log('entered makecallwins');
        var url = 'http://student12.cse.nd.edu:51046/oscars/filter/minwins/' + parseInt(wins);
        var xhr = new XMLHttpRequest();
        xhr.open("GET", url, true);

        xhr.onload = function(e){
                console.log('response received');
                displaywins(xhr.responseText);
        }
        xhr.onerror = function(e){
                console.error(xhr.statusText);
        }
        xhr.send(null);

}

function displaywins(response){
        console.log("entered display", response);
        var resp_json = JSON.parse(response);
        var resp_array = resp_json['data'];
        var resp_str = processArrayswins(resp_array);
        var filter_results = document.getElementById('wins-results');
        filter_results.innerHTML = resp_str;

}

function processArrayswins(resp_array){
        var resp_str = "";
	for (const [key, value] of Object.entries(resp_array)) {
                resp_str += `${key}` + ' has ' + `${value}` + '<br><hr>';
        }
        return resp_str;
}


function makecallNom(nom){
	console.log('entered makecallnom');
        var url = 'http://student12.cse.nd.edu:51046/oscars/filter/minnoms/' + parseInt(nom);
        var xhr = new XMLHttpRequest();
        xhr.open("GET", url, true);

        xhr.onload = function(e){
                console.log('response received');
                displaynom(xhr.responseText);
        }
        xhr.onerror = function(e){
                console.error(xhr.statusText);
        }
        xhr.send(null);

}
function displaynom(response){
        console.log("entered display", response);
        var resp_json = JSON.parse(response);
        var resp_array = resp_json['data'];
	var resp_str = processArraysnom(resp_array);
        var filter_results = document.getElementById('nom-results');
        filter_results.innerHTML = resp_str;

}

function processArraysnom(resp_array){
	var resp_str = "";
	for (const [key, value] of Object.entries(resp_array)) {
		resp_str += `${key}` + ' has ' + `${value}` + '<br><hr>';
	}
  
        return resp_str;
}

