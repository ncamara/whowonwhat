#!/usr/bin/python3
import sys
sys.path.append('..')
import unittest
from ooapi._oscars_database import _oscars_database
import requests
import json

class TestOscarsDatabase(unittest.TestCase):
	odb = _oscars_database()
	odb.load_movies('../oscars_data.json')
	url = 'http://localhost:51046/oscars/'
	reset_url = 'http://localhost:51046/reset/'

	def reset_data(self):
		m = {}
		r = requests.put(self.reset_url, json.dumps(m))


	def test_get_all(self):
		self.reset_data()
		# Grab the data at the URL
		r = requests.get(self.url)
		resp = json.loads(r.content.decode())

		# Make sure we had a successful request
		self.assertEqual(resp['result'], 'success')

		resp_items = resp['data']
		
		# Make sure that the response got all of the Oscar nominees ever

		for item1, item2 in zip(resp_items, self.odb.oscars_dict):
			self.assertEqual(item1['entity'], item2['entity'])
			self.assertEqual(item1['category'], item2['category'])
			self.assertEqual(item1['year'], item2['year'])
			self.assertEqual(item1['winner'], item2['winner'])	

	def test_get_year(self):
		self.reset_data()
		# Get all of the Oscar nominees from the year 2016
		r = requests.get(self.url + 'year/2016')
		resp = json.loads(r.content.decode())

		self.assertEqual(resp['result'], 'success')

		resp_items = resp['data']

		# Make sure that each entry is a nomination from 2016
		for item in resp_items:
			self.assertEqual(item['year'], 2016)


	def test_get_entity(self):
		self.reset_data()
		# Grab all nominations for the film 'Crash'
		r = requests.get(self.url + 'entity/Crash')
		resp = json.loads(r.content.decode())

		self.assertEqual(resp['result'], 'success')

		resp_items = resp['data']

		# Make sure that the 'entity' attribute of each nomination is the film 'Crash'
		for item in resp_items:
			self.assertEqual(item['entity'], 'Crash')


	def test_get_winners(self):
		self.reset_data()
		# Get all winners in Oscars history
		r = requests.get(self.url + 'winners/')
		resp = json.loads(r.content.decode())

		self.assertEqual(resp['result'], 'success')

		resp_items = resp['data']

		# Make sure that every entity is actually a winner
		for item in resp_items:
			self.assertEqual(item['winner'], 'TRUE')

	
	def test_get_year_winners(self):
		self.reset_data()
		r = requests.get(self.url + 'winners/year/2010')
		resp = json.loads(r.content.decode())

		self.assertEqual(resp['result'], 'success')

		resp_items = resp['data']

		for item in resp_items:
			self.assertEqual(item['year'], 2010)
			self.assertEqual(item['winner'], 'TRUE')


	def test_get_entity_wins(self):
		self.reset_data()
		# Grab all of the Oscars that the film 'Avatar' won.
		# Note that if you wanted to put a movie with space or an actor/actress, 
		# the %20 character to represent a space must be used in place of usual spaces.
		r = requests.get(self.url + 'winners/entity/Avatar')
		resp = json.loads(r.content.decode())

		self.assertEqual(resp['result'], 'success')

		resp_items = resp['data']

		# Make sure that each item is for the entity 'Avatar' and is actually a winner
		for item in resp_items:
			self.assertEqual(item['entity'], 'Avatar')
			self.assertEqual(item['winner'], 'TRUE')
	
	
	def test_get_min_wins(self):
		self.reset_data()
		# Grab all of the movies/actors that have a at least 4 Oscar wins
		r = requests.get(self.url + 'filter/minwins/4')
		resp = json.loads(r.content.decode())

		self.assertEqual(resp['result'], 'success')

		resp_items = resp['data']

		# Make sure that each entry has at least 4 wins
		for wins in resp_items.values():
			self.assertGreaterEqual(wins, 4)


	def test_get_min_noms(self):
		self.reset_data()
		# Grab all of the movies/actors that have at least 10 nominations
		r = requests.get(self.url + 'filter/minnoms/10')
		resp = json.loads(r.content.decode())

		self.assertEqual(resp['result'], 'success')

		resp_items = resp['data']

		# Make sure that each returned movie does have at least 10 nominations
		for wins in resp_items.values():
			self.assertGreaterEqual(wins, 10)

	
	def test_put_comment(self):
		self.reset_data()
		input_comment = 'This movie makes me cry!'
		msg = {'year' : 2016, 'entity' : 'Moonlight', 'category' : 'BEST PICTURE', 'comment' : input_comment}
			
		r = requests.put(self.url + 'film/comment/', data=json.dumps(msg))
		resp = json.loads(r.content.decode())

		
		self.assertEqual(resp['result'], 'success')

		r = requests.get(self.url + 'entity/Moonlight')
		resp = json.loads(r.content.decode())

		items = resp['data']

		for item in items:
			if item['entity'] == 'Moonlight' and item['category'] == 'BEST PICTURE':
				self.assertEqual(item['comments'][0], input_comment)

		


if __name__ == '__main__':
	unittest.main()
