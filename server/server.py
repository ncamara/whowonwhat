#!/usr/bin/python3
import sys
sys.path.append("..")
import cherrypy
from ooapi._oscars_database import _oscars_database
from oscars_controller import oscars_controller
def start_service():

	# Initialize dispatcher, oscars database object, and our controller
	dispatcher = cherrypy.dispatch.RoutesDispatcher()
	odb = _oscars_database()
	o_controller = oscars_controller(odb=odb)


	# Connect the dispatcher appropriately
	dispatcher.connect('odb_get_all', '/oscars/',controller=o_controller,action='GET_ALL',conditions=dict(method=['GET']))
	dispatcher.connect('odb_get_year', '/oscars/year/:year',controller=o_controller,action='GET_YEAR',conditions=dict(method=['GET']))
	dispatcher.connect('odb_get_entity', '/oscars/entity/:entity',controller=o_controller,action='GET_ENTITY',conditions=dict(method=['GET']))
	dispatcher.connect('odb_get_winners', '/oscars/winners/',controller=o_controller,action='GET_WINNERS',conditions=dict(method=['GET']))
	dispatcher.connect('odb_get_year_winners', '/oscars/winners/year/:year',controller=o_controller,action='GET_YEAR_WINNERS',conditions=dict(method=['GET']))
	dispatcher.connect('odb_get_entity_wins', '/oscars/winners/entity/:entity',controller=o_controller,action='GET_ENTITY_WINS',conditions=dict(method=['GET']))
	dispatcher.connect('odb_get_min_wins', '/oscars/filter/minwins/:minWins',controller=o_controller,action='GET_MIN_WINS',conditions=dict(method=['GET']))
	dispatcher.connect('odb_get_min_noms', '/oscars/filter/minnoms/:minNoms',controller=o_controller,action='GET_MIN_NOMS',conditions=dict(method=['GET']))
	dispatcher.connect('odb_put_comment', '/oscars/film/comment/',controller=o_controller,action='PUT_COMMENT',conditions=dict(method=['PUT']))
	dispatcher.connect('odb_post_args', '/oscars/filter/',controller=o_controller,action='POST_ARGS',conditions=dict(method=['POST']))
	dispatcher.connect('reset', '/reset/',controller=o_controller,action='RESET',conditions=dict(method=['POST']))

	dispatcher.connect('oscars_options', '/oscars/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
	 
	# Set up configuration
	conf = {'global':{'server.socket_host':'student12.cse.nd.edu','server.socket_port':51046,},'/':{'request.dispatch':dispatcher, 'tools.CORS.on' : True}}
	cherrypy.config.update(conf)
	app = cherrypy.tree.mount(None,config=conf)

	# Start server
	cherrypy.quickstart(app)



class optionsController:
	def OPTIONS(self, *args, **kwargs):
		return ""

def CORS():
	cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
	cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
	cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"

if __name__ == '__main__':
	cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
	start_service()
