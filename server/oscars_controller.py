import cherrypy
import re, json
from ooapi._oscars_database import _oscars_database

class oscars_controller(object):

	ACTING_CATEGORIES = ['ACTOR IN A LEADING ROLE',
						 'ACTRESS IN A LEADING ROLE',
						 'ACTOR IN A SUPPORTING ROLE',
						 'ACTRESS IN A SUPPORTING ROLE']

	def __init__(self, odb=None):
		if odb is None:
			self.odb = _oscars_database()
		else:
			self.odb = odb
		self.odb.load_movies('../oscars_data.json')


	def RESET(self):
		output = {'result' : 'success'}
		self.odb = self.odb.load_movies('../oscars_data.json')
		return json.dumps(output)


	# Return the user a json response will all Oscar nominees
	def GET_ALL(self):
		output = {'result' : 'success'}
		
		all_data = self.odb.get_all()

		if all_data:
			output['data'] = all_data
		else:
			output['result'] = 'error'
			output['message'] = 'Sorry! Something went wrong on our end. Please Try Again'

		return json.dumps(output)


	# Return the user a json response with all nominees from specified year
	def GET_YEAR(self, year):
		year = int(year)	
		output = {'result' : 'success'}

		year_data = self.odb.get_year(year)

		if year_data:
			output['data'] = year_data
		else:
			output['result'] = 'error'
			output['message'] = 'We couldn\'t find any Oscar nominees from that year'

		return json.dumps(output)


	# Return the user a json response with all nominations of specified entity
	def GET_ENTITY(self, entity):
		output = {'result' : 'success'}
		entity_data = self.odb.get_entity(entity)

		if entity_data:
			output['data'] = entity_data
		else:
			output['result'] = 'error'
			output['message'] = 'We couldn\'t find any Oscar nominees for that entity'

		return json.dumps(output)


	# Return the user a json repsonse with all Oscar winners
	def GET_WINNERS(self):
		output = {'result' : 'success'}
		winners_data = self.odb.get_winners()

		if winners_data:
			output['data'] = winners_data
		else:
			output['result'] = 'error'
			output['message'] = 'Sorry! Something went wrong on our end. Please try again.'

		return json.dumps(output)


	# Return the user a json response with all Oscar winners from the specified year
	def GET_YEAR_WINNERS(self, year):
		year = int(year)
		output = {'result' : 'success'}
		year_winner_data = self.odb.get_year_winners(year)

		if year_winner_data:
			output['data'] = year_winner_data
		else:
			output['result'] = 'error'
			output['message'] = 'We couldn\'t find any Oscar winners from that year'

		return json.dumps(output)

	# Return the user a json response with all Oscars the specified entity won
	def GET_ENTITY_WINS(self, entity):
		output = {'result' : 'success'}
		entity_wins_data = self.odb.get_entity_wins(entity)

		if entity_wins_data:
			output['data'] = entity_wins_data
		else:
			output['result'] = 'error'
			output['message'] = 'We couldn\'t find any Oscar wins from that entity'

		return json.dumps(output)


	# Returns the user a json response with all entities who have had at least
	# the minimum number of wins specified
	def GET_MIN_WINS(self, minWins):
		output = {'result' : 'success'}
		min_wins_data = self.odb.get_min_wins(minWins)

		if min_wins_data:
			output['data'] = min_wins_data
		else:
			output['result'] = 'error'
			output['message'] = 'We couldn\'t find an entity with that many Oscar wins.'

		return json.dumps(output)


	# Returns the user a json response with all entities who have had at least
	# the minimum number of nominations specified
	def GET_MIN_NOMS(self, minNoms):
		output = {'result' : 'success'}
		min_noms_data = self.odb.get_min_nominations(minNoms)

		if min_noms_data:
			output['data'] = min_noms_data
		else:
			output['result'] = 'error'
			output['message'] = 'We couldn\'t find an entity with that many Oscar nominations.'

		return json.dumps(output)

	# Adds a comment to the given entity's nomination.
	# POST request should look something like:
	# 	{'year' : 2005, 
	#    'entity' : 'Crash',
	#	 'category' : 'BEST PICTURE',
	#	 'comment' : 'I love this movie!'}
	def PUT_COMMENT(self):
		output = {'result' : 'success'}
		
		data_str = cherrypy.request.body.read()
		data_json = json.loads(data_str)
		if 'year' in data_json.keys() and 'entity' in data_json.keys() and 'category' in data_json.keys():
			for entity in self.odb.oscars_dict:
				if entity['category'] == data_json['category'] and entity['year'] == data_json['year'] and entity['entity'] == data_json['entity']:
					comment = data_json['comment']
					entity['comments'].append(comment)
		else:
			output = {'result' : 'error', 'message' : 'You must specify a year, category, and entity name in your request'}
		#except Exception as ex:
		#	output['result'] = 'error'
		#	output['message'] = str(ex)

		return json.dumps(output)

	

