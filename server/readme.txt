Instructions to run server tests --> python3 test_oscars_server.py
port number = 51046

we are hoping this will serve not only as a search engine for the Oscars specifically, 
but also allow people to recognize patterns more easily. Having the ability to have the 
data parsed by varying entry points allows us to show what actors/actresses/franchises/companies 
are strong contenders for an Oscar based on what they may have won prior. It will also allow 
people to make comments and filter by minimum wins or nominations

