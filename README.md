# WhoWonWhat

ncamara@nd.edu  vgoyette@nd.edu
Paradigms Final Project

JSON Specification:
oscars_data.json

OOAPI Description:
Instructions to run API test → python3 test_api.py

We first connect the handlers written in _oscars_database.py to the oscars_controller.py in the server folder. These handlers deal with how to pull the data needed from the json, and the controllers handle the response that will return to the server. The API utilizes cherrypy to connect the controller to server.py through the dispatcher method, and we connect each method in the controller to the server.

Server Description:
Instructions to run server tests --> python3 test_oscars_server.py
port number = 51046
http://ncamara.gitlab.io/whowonwhat

User Interaction:
The user can use the categories page to click on specific categories that they are interested in. Then from that they go to another page in which all of the years are listed as a accordian and they can view each options results

Web client tests:
We tested our web clients by going through the results that they produced and made sure that the results checked out against internet data or we can use our api itself and filter based on our already checked functions.

Scale and Complexity: 
Our website tested us as it was our first web client on the basis of how to make a website.
We were able to investigate different options while not getting overwhelmed with more advanced features.
Our project can be scaled not only to include more options and services to the user, but can expand to other TV/Movie award shows and then can be expanded to music award shows.

Slides:  https://docs.google.com/presentation/d/1q85mCMZarNY2-zDig4BVPMclmGzha1ML6qeopnfq8C8/edit?usp=sharing 

we are hoping this will serve not only as a search engine for the Oscars specifically, 
but also allow people to recognize patterns more easily. Having the ability to have the 
data parsed by varying entry points allows us to show what actors/actresses/franchises/companies 
are strong contenders for an Oscar based on what they may have won prior. It will also allow 
people to make comments and filter by minimum wins or nominations



