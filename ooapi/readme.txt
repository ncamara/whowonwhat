OOAPI Description:
Instructions to run API test → python3 test_api.py

We first connect the handlers written in _oscars_database.py to the oscars_controller.py in the server folder. 
These handlers deal with how to pull the data needed from the json, 
and the controllers handle the response that will return to the server. 
The API utilizes cherrypy to connect the controller to server.py through the dispatcher method, 
and we connect each method in the controller to the server.
