#!/usr/bin/python3
import json
import cherrypy

class _oscars_database:

	def __init__(self):
		# This list will be our main "database" for this project
		oscars_dict = dict()

	# Loads the Oscars data into a dictionary
	def load_movies(self, data_file):
		with open(data_file) as file:
			self.oscars_dict = json.load(file)
		for entry in self.oscars_dict:
			entry['comments'] = []

	# Get all entries in the dataset
	def get_all(self):
		return self.oscars_dict
	
	# Get all Oscar nominees from a specified year
	def get_year(self, year):
		match_list = []
		for movie in self.oscars_dict:
			if movie['year'] == year:
				match_list.append(movie)
		return match_list

	# Get all Oscar nominations for a specific actor/actress/movie/director/etc.
	def get_entity(self, entity):
		match_list = []
		for movie in self.oscars_dict:
			if str(movie['entity']).lower() == entity.lower():
				match_list.append(movie)
		return match_list

	# Get all entities that have won an Oscar
	def get_winners(self):
		winners_list = []
		for movie in self.oscars_dict:
			if movie['winner'] == 'TRUE':
				winners_list.append(movie)
		return winners_list

	# Returns all winners from the specified year
	def get_year_winners(self, year):
		winners_list = []
		for movie in self.oscars_dict:
			if movie['winner'] == 'TRUE' and movie['year'] == year:
				winners_list.append(movie)
		return winners_list

	def get_entity_wins(self, entity):
		entity_list = []
		for movie in self.oscars_dict:
			if movie['winner'] == 'TRUE' and str(movie['entity']).lower() == entity.lower():
				entity_list.append(movie)
		return entity_list

	def get_min_wins(self, minWins):
		win_counts = {}
		for entry in self.oscars_dict:
			name = entry['entity']
			
			if entry['winner'] == 'TRUE':
				if name in win_counts.keys():
					win_counts[name] += 1
				else:
					win_counts[name] = 1
		
		delete = []
		for key, value in win_counts.items():
			if int(value) < int(minWins):
				delete.append(key)

		for i in delete:
			del win_counts[i]

		return win_counts

	def get_min_nominations(self, minNoms):
		nom_counts = {}
		for entry in self.oscars_dict:
			name = entry['entity']

			if name in nom_counts.keys():
				nom_counts[name] += 1
			else:
				nom_counts[name] = 1

		delete = []
		for key, value in nom_counts.items():
			if int(value) < int(minNoms):
				delete.append(key)

		for i in delete:
			del nom_counts[i]

		return nom_counts



	def put_comment(self, entity):
		output = {'result':'success'}
		try:
			data_str = cherrypy.request.body.read()
			data_json = json.loads(data_str)
			comment = data_json['comment']
			comment = str(comment)
			self.oscars_dict.entity.comments.append(comment)
		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)
		return json.dumps(output)


	'''def post_args(self):
		output = {'result':'success'}
		try:
			data_str = cherrypy.request.body.read()
			data_json = json.loads(data_str)
			if data_json.has_key('minWins'):
				min_val = data_json['minWins']
				if data_json['category'] == 'acting':
					actors = {}
					for entry in self.oscars_dict:
						if (entry['category'] == 'ACTOR' and entry['winner'] == 'TRUE') or (entry['category'] == 'ACTRESS' and entry['winner'] == 'TRUE'):
							name = entry['entity']
							if actors.has_key(name):
								num = actors[name]
								num+=1
								actors[name] = num
							else:
								actors[name] = 1
					minwins_list = []
					for key, value in actors.items():
						if value >= min_val:
							minwins_list.append(key)
			elif data_json.has_key('minNoms'):
				min_val = data_json['minNoms']
				if data_json['category'] == acting:
					actors = {}
					for entry in self.oscars_dict:
						if (entry['category'] == 'ACTOR') or (entry['category'] == 'ACTRESS'):
							name = entry['entity']
							if actors.has_key(name):
								num = actors[name]
								num+=1
								actors[name] = num
							else:
								actors[name] = 1
					minnom_list = []
					for key, value in actors.items():
						if value >= min_val:
							minnom_list.append(key)
		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)
		return json.dumps(output)'''

