#!/usr/bin/python3
import unittest
from _oscars_database import _oscars_database

class TestOscarsDatabase(unittest.TestCase):
	odb = _oscars_database()
	odb.load_movies('../oscars.json')

	def test_load_movies(self):
		test_obd = _oscars_database()
		test_obd.load_movies('../oscars.json')
		for movie in test_obd.oscars_dict:

			# Check that each entry has the expected keys using a generator
			# This statement will only evaluate to true if all the expected
			# categories are in the keys of the current movie.
			hasKeys = all(x in movie.keys() 
							for x in ['year', 'category', 'winner', 'entity'])
			
			self.assertEqual(hasKeys, True)
	
	def test_get_all(self):
		# Load the movies into a new object
		test_obd = _oscars_database()
		test_obd.load_movies('../oscars.json')

		# Retrieve all movies and store in new_dict
		new_dict = self.odb.get_all()

		# Pick some random entry and make sure they are equal in both objects
		self.assertEqual(test_obd.oscars_dict[450], new_dict[450])

	def test_get_year(self):
		# Get movies from 2015 into new variable
		oscars_2015 = self.odb.get_year(2015)

		# Go thru each movie in new list and make sure their year is 2015
		for nom in oscars_2015:
			self.assertEqual(nom['year'], 2015)

	def test_get_entity(self):
		# We'll grab the Oscar nominations for Titanic
		titanic_oscars = self.odb.get_entity('Titanic')

		# We then make sure that all returned entities are for Titanic
		for nom in titanic_oscars:
			self.assertEqual(nom['entity'], 'Titanic')

	def test_get_winners(self):
		# Grab all of the Oscar winners all time
		winners = self.odb.get_winners()

		# Make sure that each returned entity was in fact a winner
		for winner in winners:
			self.assertEqual(winner['winner'], 'TRUE')
	
	def test_get_year_winners(self):
		# Grab all Oscar winners from the year 2014
		year_winners = self.odb.get_year_winners(2014)

		# Make sure each entity is a winner and is from 2014
		for entity in year_winners:
			self.assertEqual(entity['year'], 2014)
			self.assertEqual(entity['winner'], 'TRUE')

	def test_get_entity_wins(self):
		# Grab all wins for a certain entity (movie or actor/actress)
		# In this case we will get all wins for the movie Moonlight
		entity_wins = self.odb.get_entity_wins('Moonlight')

		# Make sure each entity is Moonlight and that it's the winners only
		for win in entity_wins:
			self.assertEqual(win['entity'], 'Moonlight')
			self.assertEqual(win['winner'], 'TRUE')


if __name__ == '__main__':
	unittest.main()
